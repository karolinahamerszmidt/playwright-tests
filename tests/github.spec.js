const { test, expect } = require("@playwright/test");

test("Karolina Hamerszmidt repo exists", async ({ page }) => {
  await page.goto("https://github.com/");

  await page.fill("input[name='q']", "karolinahamerszmidt");

  await page.press("input[name='q']", "Enter");

  await page.click("text=karolinahamerszmidt");

  const headerText = await page.textContent("#readme h1");

  expect(headerText).toBe("Hi , I'm Karolina Hamerszmidt");
});
